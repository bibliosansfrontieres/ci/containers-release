# CI : Containers release

Will release project containers to Docker Hub and Gitlab Hub.

## Usage

```
include:
  - project: 'bibliosansfrontieres/ci/containers_release'
    file: 'template.yml'
```

## Override variables

|Variable|Description|Default|Required|
|-|-|-|-|
|DOCKER_HUB_USER|Docker Hub username| |yes|
|DOCKER_HUB_PASSWORD|Docker Hub password| |yes|
|DOCKER_HUB_REPOSITORY|Docker Hub repository| |yes|
|IMAGE_NAME|Image name| |yes|
|IMAGE_VERSION|Image version| |yes|
|BUILD_ARCH_LIST|Build architectures list|linux/amd64|no|

Available architectures :
linux/amd64,linux/arm64

## Choose stage

By default, `containers_release` uses `release` stage.

You can override this by adding following code to your `.gitlab-ci.yml` :

```
containers_release:
  stage: THE_STAGE_YOU_WANT_TO_USE
```